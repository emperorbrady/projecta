package com.amazingbrady.discretecalculator;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.math.BigInteger;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

public class BasicCalc extends ActionBarActivity {


    final String error = "Error";
    final String defBase = "2";
    HashMap<Character,Integer> priorities = new HashMap<Character,Integer>();
    TextView buffer;
    Deque<Character> tokens = new ArrayDeque<Character>();
    Stack<String> toClose = new Stack<String>();
    Stack<Integer> openOnLevel = new Stack<Integer>();
    TextView modulo;
    boolean inBase = false;
    boolean insertState = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buttons);
        buffer = (TextView)findViewById(R.id.buffer);
        modulo = (TextView)findViewById(R.id.modulo);
        Button log = (Button) findViewById(R.id.buttonLog);
        Button del = (Button) findViewById(R.id.buttonDel);
        log.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                handleHold(view);
                return true;
            }
        });
        del.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                handleHold(view);
                return true;
            }
        });

        initPrecedence();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.basic_calc, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    public void handleClick(View v) {
        Button b = (Button)findViewById(v.getId());
        String bufferText = buffer.getText().toString().trim();
        String moduloText = modulo.getText().toString().trim();
        String buttonText = b.getText().toString();
        switch(v.getId()) {
            case R.id.buttonOne:
            case R.id.buttonTwo:
            case R.id.buttonThree:
            case R.id.buttonFour:
            case R.id.buttonFive:
            case R.id.buttonSix:
            case R.id.buttonSeven:
            case R.id.buttonEight:
            case R.id.buttonNine:
            case R.id.buttonZero:
            case R.id.buttonOpen:
            case R.id.buttonClose:
                if(!insertState) {
                    clean();
                    insertState = true;
                }
                tokens.push(buttonText.charAt(0));
                buffer.setText(Html.fromHtml(parseTokens()));
                break;
            case R.id.buttonPlus:
            case R.id.buttonMinus:
            case R.id.buttonStar:
            case R.id.buttonInv:
                if(bufferText.isEmpty()) {
                    if (v.getId() == R.id.buttonPlus || v.getId() == R.id.buttonMinus) {
                        tokens.push(buttonText.charAt(0));
                    }
                } else {
                    tokens.push(buttonText.charAt(0));
                }
                buffer.setText(Html.fromHtml(parseTokens()));
                break;
            case R.id.buttonEquals:
                buffer.setText(parseInput(parseTokens(), moduloText));
                insertState = false;
                ((Button)findViewById(R.id.buttonDel)).setText("CLR");
                break;
            case R.id.setModulo:
                String m = parseInput(parseTokens(), "");
                clean();
                modulo.setText(m.equals(error) ? "" : m);
                break;
            case R.id.buttonClear:
                clean();
                insertState = true;
                break;
            case R.id.buttonLog:
                tokens.push(getResources().getString(R.string.logDefBase).charAt(0));
                buffer.setText(Html.fromHtml(parseTokens()));
                insertState = true;
                break;
            case R.id.buttonExp:
                tokens.push(buttonText.charAt(0));
                buffer.setText(Html.fromHtml(parseTokens()));
                insertState = true;
                break;
            case R.id.buttonDel:
                if(insertState) {
                    if(bufferText.length() > 0) {
                        tokens.pop();
                        buffer.setText(Html.fromHtml(parseTokens()));
                    }
                } else {
                    clean();
                    insertState = true;
                }
            default:
                break;




        }
    }

    public void handleHold(View v) {
        switch(v.getId()) {
            case R.id.buttonLog:
                tokens.push(getResources().getString(R.string.logSetBase).charAt(0));
                buffer.setText(Html.fromHtml(parseTokens()));
                break;
            case R.id.buttonDel:
                clean();
                break;
            default:
                break;

        }

    }

    private void initPrecedence() {
        priorities.put('\u002b', 1);
        priorities.put('\u2212', 1);
        priorities.put('\u00F7', 2);
        priorities.put('\u00D7', 2);
        priorities.put('^', 3);
    }


    private BigInteger calc(BigInteger a, BigInteger b, char op, BigInteger base) {

        BigInteger val;
        switch(op) {
            case '\u002b':
                val = a.add(b);
                break;
            case '\u2212':
                val = a.subtract(b);
                break;
            case '\u00F7':
                val = base != null ? a.multiply(b.modInverse(base)) : a.divide(b);
                break;
            case '\u00D7':
                val = a.multiply(b);
                break;
            default:
                val = new BigInteger("0");
                break;
        }
        return base != null ? val.mod(base) : val;
    }
    private String parseInput(String s, String baseString) {
        ArrayList<String> rpn = toRPN(s);
        for(String ss: rpn) {
            Log.i("RPN", ss);
        }
        Log.i("BASE", baseString + "END");
        BigInteger base = !baseString.isEmpty() ? new BigInteger(baseString) : null;
        if(rpn == null) {
            return error;
        }
        Stack<BigInteger> stack = new Stack<BigInteger>();
        for(String c: rpn) {
            if (!isValidOperation(c)) {
                if(base == null) stack.push(new BigInteger(c));
                else stack.push((new BigInteger(c).mod(base)));
            } else {
                if(stack.size() < 2) return error;
                BigInteger t = stack.pop();
                stack.push(calc(stack.pop(), t, c.charAt(0), base));
            }
        }

        return stack.isEmpty() ? error : stack.pop().toString();
    }


    private ArrayList<String> toRPN(String s) {
        ArrayList<String> queue = new ArrayList<String>();
        Stack<Character> stack = new Stack<Character>();
        String number = "";
        char[] chars = s.toCharArray();
        for(int i = 0; i < chars.length; i++) {
            char c = chars[i];
            if(isNumber(c)) {
                number = number + c;
            } else {
                if(!number.isEmpty()) queue.add(number);
                if(isValidOperation(c)) {
                    int t = priorities.get(c);
                    while(stack.size() > 0 && isValidOperation(stack.peek()) &&
                            t < priorities.get(stack.peek())) {
                        queue.add(String.valueOf(stack.pop()));
                    }
                    stack.push(c);
                } else if(c == '(') {
                    if(i > 0 && isNumber(chars[i-1])) {
                        while(stack.size() > 0 && isValidOperation(stack.peek()) &&
                                2 < priorities.get(stack.peek())) {
                            queue.add(String.valueOf(stack.pop()));
                        }
                        stack.push('\u00d7');
                    }
                    stack.push(c);
                } else if(c == ')') {
                    while(stack.peek() != '(') {
                        if(stack.empty()) {
                            return null;
                        }
                        queue.add(String.valueOf(stack.pop()));
                    }
                    stack.pop();
                    if(i < chars.length - 1 && isNumber(chars[i+1])) {
                        while(stack.size() > 0 && isValidOperation(stack.peek()) &&
                                2 < priorities.get(stack.peek())) {
                            queue.add(String.valueOf(stack.pop()));
                        }
                        stack.push('\u00d7');
                    }
                }
                number = "";
            }
        }
        if (!number.isEmpty()) queue.add(number);
        while(!stack.empty()) {
            char t = stack.pop();
            if(t == '(' || t == ')') {
                return null;
            }
            queue.add(String.valueOf(t));
        }
        return queue;
    }

    private boolean isValidOperation(char c) {
        return (c == '\u002b' || c == '\u2212' || c == '\u00F7' || c == '\u00D7');
    }

    private boolean isValidOperation(String c) {
        return c.length() == 1 && isValidOperation(c.charAt(0));
    }

    private boolean isNumber(char c) {
        return (c >= '0' && c <= '9');
    }


    private String parseTokens() {
        String val = "";
        Stack<AtomicInteger> openOnLevel = new Stack<AtomicInteger>();
        Stack<String> toClose = new Stack<String>();
        boolean inBase = false;
        for (char t: tokens) {

            if (isNumber(t)) {
                val += t;
            } else if (isValidOperation(t)) {
                if (inBase) {
                    val += toClose.pop();
                    inBase = false;
                } else if (openOnLevel.isEmpty()) {
                    val += t;
                } else if (!openOnLevel.isEmpty() && openOnLevel.peek().get() == 0) {
                    val += toClose.pop() + t;
                    openOnLevel.pop();
                } else if (!openOnLevel.isEmpty() && openOnLevel.peek().get() > 0) {
                    val += t;
                }
            } else if (t == '(' || t == ')') {
                if (inBase) {
                    val += toClose.pop();
                    inBase = false;
                } else if (openOnLevel.isEmpty()) {
                    val += t;
                } else if (!openOnLevel.isEmpty()) {
                    if (t == '(') openOnLevel.peek().incrementAndGet();
                    else openOnLevel.peek().decrementAndGet();
                    val += t;
                }
            } else if (t == '\u0192') { //log with base
                if (inBase) {
                    val += toClose.pop();
                    inBase = false;
                } else if (openOnLevel.isEmpty() || openOnLevel.peek().get() > 0) {
                    val += "log<sub><small>";
                    toClose.push("</small></sub>");
                    inBase = true;
                } else if (openOnLevel.peek().get() == 0) {
                    val += toClose.pop() + "log<sub><small>";
                    toClose.push("</small></sub>");
                    openOnLevel.pop();
                    inBase = true;
                }
            } else if (t == '\u0260') { //log with def. base
                if (inBase) {
                    val += toClose.pop();
                    inBase = false;
                } else if (openOnLevel.isEmpty() || openOnLevel.peek().get() > 0) {
                    val += "log";
                } else if (openOnLevel.peek().get() == 0) {
                    val += toClose.pop() + "log";
                    openOnLevel.pop();
                }
            } else if (t == ',') {
                val += toClose.pop() + '(';
            } else if (t == '^') {
                if(inBase) {
                    val += toClose.pop();
                    inBase = false;
                } else {
                    val += "<sup><small>^";
                    toClose.push("</small></sup>");
                    openOnLevel.push(new AtomicInteger(0));
                }
            }
        }
        return val;
    }

    private String parseClosingTags() {
        String val = "";
        for(char t: tokens) {
            val = val + t;
        }
        return val;
    }

    private void insertPrep() {
        if(!insertState) {
            insertState = true;
            ((Button)findViewById(R.id.buttonDel)).setText("DEL");
        }
    }

    private void clean() {
        tokens.clear();
        toClose.empty();
        buffer.setText("");
    }
}
